"""excel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.urls import include, path
	2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views

import app.views
import app.forms

urlpatterns = [
	path('admin/', admin.site.urls),
	url(r'^$', app.views.upload_file, name='home'),
	url(r'^upload$', app.views.upload_file, name='upload'),
	url(r'^list-files/$', app.views.list_files, name='list_files'),
	url(r'^list-files/(?P<page_number>\d+)$', app.views.list_files, name='list_files'),
	url(r'^file-data/$', app.views.file_data, name='file_data'),
	url(r'^file-data/(?P<file_id>\d+)$', app.views.file_data, name='file_data'),
	url(r'^file-data/(?P<file_id>\d+)/(?P<page_number>\d+)$', app.views.file_data, name='file_data'),
]
