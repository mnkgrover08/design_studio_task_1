"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".
"""

import django
from django.test import TestCase
from app.models import Files, FilesData

class ViewTest(TestCase):
	"""Tests for the application views."""

	if django.VERSION[:2] >= (1, 7):
		# Django 1.7 requires an explicit setup() when running tests in PTVS
		@classmethod
		def setUpClass(cls):
			super(ViewTest, cls).setUpClass()
			django.setup()

	def test_upload(self):
		"""Tests the upload page."""
		response = self.client.get('/upload')
		self.assertContains(response, 'Upload Form', 1, 200)

	def test_files_listing(self):
		"""Tests the files listing page."""
		response = self.client.get('/list-files')
		self.assertContains(response, 'Files Listing', 0, 301)

	def test_files_data(self):
		"""Tests the files data page."""
		response = self.client.get('/file-data')
		self.assertContains(response, 'File Data', 0, 301)