
import xlrd
import openpyxl
from .models import Files, FilesData
from datetime import datetime
from django.core.paginator import Paginator


def handle_file_data(file_id,page_number):
	file_data = FilesData.objects.filter(file_id=file_id)
	head_columns = set()
	for file in file_data:
		head_columns.add(file.column)
	column_len = len(head_columns)
	counter,second_counter = 1,1
	tmp_dict = {}
	final_list = []
	for file in file_data:
		tmp_dict[file.column] = file.value
		if counter%column_len==0:
			#tmp_dict['created_on'] = file.created_on
			#tmp_dict['updated_on'] = file.updated_on
			final_list.append(tmp_dict)
			tmp_dict = {}
			second_counter += 1
		counter += 1
	
	paginator = Paginator(final_list, 10)
	data = {}

	try:
		data = paginator.page(page_number)
	except PageNotAnInteger:
		data = paginator.page(1)
	except EmptyPage:
		data = paginator.page(paginator.num_pages)
	except Exception as e:
		data = {}

	return {'data':data,'final_list':final_list}

def handle_files_listing(page_number):
	files_list = Files.objects.all()
	paginator = Paginator(files_list, 10)
	try:
		files = paginator.page(page_number)
	except PageNotAnInteger:
		files = paginator.page(1)
	except EmptyPage:
		files = paginator.page(paginator.num_pages)

	return files

def handle_uploaded_file(excel_file,file_name):
	"""handle file upload function"""
	workbook = xlrd.open_workbook(file_contents=excel_file.read())
	worksheet = workbook.sheet_by_index(0)
	
	if int(worksheet.nrows) <= 0 or int(worksheet.ncols) <= 0:
		status = 'error'
		code = 'EXCEL_EMPTY'
		return {'status':status,'code':code}
	
	if int(worksheet.nrows) > 1000:
		status = 'error'
		code = 'ROWS_LIMIT_EXCEEDED'
		return {'status':status,'code':code}

	if int(worksheet.ncols) > 100:
		status = 'error'
		code = 'COLUMN_LIMIT_EXCEEDED'
		return {'status':status,'code':code}

	file_object = Files(file_name=file_name,created_on=datetime.now(),updated_on=datetime.now())
	file_object.save()
	file_id = file_object.id
	
	keys = [worksheet.cell(0, col_index).value for col_index in range(worksheet.ncols)]

	dict_list = []

	for row_index in range(1, worksheet.nrows):
		d = {keys[col_index]: worksheet.cell(row_index, col_index).value 
			 for col_index in range(worksheet.ncols)}
		dict_list.append(d)

	for row in dict_list:
		for column_name,column_value in row.items():
			files_data_object = FilesData(file=file_object,column=column_name,value=column_value,created_on=datetime.now(),updated_on=datetime.now())
			files_data_object.save()

	return {'status':'success','code':'SUCCESSFUL','file_id':file_id,'file_name':file_name}