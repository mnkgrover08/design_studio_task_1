"""
Definition of forms.
"""

from django import forms
from django.core.validators import FileExtensionValidator



class UploadForm(forms.Form):
	"""Upload form which uses boostrap CSS."""
	file = forms.FileField(label= "Choose excel to upload",validators=[FileExtensionValidator(allowed_extensions=['xls','xlsx','xlsm'])])