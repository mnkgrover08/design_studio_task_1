# Generated by Django 2.2 on 2019-04-27 15:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Files',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_name', models.CharField(max_length=30)),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='FilesData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('column', models.TextField()),
                ('value', models.TextField()),
                ('created_on', models.DateField()),
                ('updated_on', models.DateField()),
                ('file', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Files')),
            ],
        ),
    ]
