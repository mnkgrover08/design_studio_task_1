from django.db import models

class Files(models.Model):
	class Meta:
		unique_together = (('id', 'file_name'),)
	file_name = models.CharField(max_length=30)
	created_on = models.DateField()
	updated_on = models.DateField()


class FilesData(models.Model):
	file = models.ForeignKey(Files, on_delete=models.CASCADE)
	column = models.TextField()
	value = models.TextField()
	created_on = models.DateField()
	updated_on = models.DateField()