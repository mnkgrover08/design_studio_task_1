"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect, JsonResponse
from datetime import datetime
from .forms import UploadForm
from .handler import handle_uploaded_file,handle_files_listing,handle_file_data
from django.core.paginator import Paginator

def file_data(request,file_id=None,page_number=1):
	"""list files function"""
	assert isinstance(request, HttpRequest)
	try:
		title = 'File Data'
		if file_id is None:
			HttpResponseRedirect('/')
		file_data = handle_file_data(file_id,page_number)
	except Exception as e:
		title = str(e)
		file_data = {'data':{},'final_list':{}}

	return render(request, 'app/file_data.html', {'date_time': datetime.now(),'title':title,'file_id':file_id,'file_data':file_data['data'],'final_list':file_data['final_list']})

def list_files(request,page_number=1):
	"""list files function"""
	assert isinstance(request, HttpRequest)
	try:
		title = 'Files Listing'
		files_listing = handle_files_listing(page_number)
	except Exception as e:
		title = str(e)
		files_listing = {}
	return render(request, 'app/file_list.html', {'date_time': datetime.now(),'title':title,'files_listing':files_listing})

def upload_file(request):
	"""upload file view definition"""
	assert isinstance(request, HttpRequest)
	
	try:
		title = 'Upload Form'
		if request.method == 'POST':
			form = UploadForm(request.POST, request.FILES)
			handle_response = {'status':'error','code':'INVALID_FORM'}
			if form.is_valid():
				handle_response = handle_uploaded_file(request.FILES['file'],request.FILES['file'].name)
			#return JsonResponse(handle_response)
			return HttpResponseRedirect('/list-files/1')
		else:
			form = UploadForm()		
	except Exception as e:
		title = str(e)
		form = None
	
	return render(request, 'app/upload.html', {'date_time': datetime.now(),'title': title,'form': form})